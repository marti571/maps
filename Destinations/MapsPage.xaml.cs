﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Destinations
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapsPage : ContentPage
    {
        
        public MapsPage()
        {
            InitializeComponent();
            var initalLocation = MapSpan.FromCenterAndRadius(new Position(33.1307785, -117.1601826), Distance.FromMiles(1));
            MyMap.MoveToRegion(initalLocation);

            PlaceAMarker();
            PopulatePicker();

            var street = new Button { Text = " Street "};
            var hybrid = new Button { Text = " Hybrid " };
            var satellite = new Button { Text = " Satellite " };
            hybrid.BorderColor = Color.White;
            satellite.BorderColor = Color.White;
            street.BorderColor = Color.White;
            hybrid.BorderWidth = 2;
            satellite.BorderWidth = 2;
            street.BorderWidth = 2;
            street.TextColor = Color.White;
            hybrid.TextColor = Color.White;
            satellite.TextColor = Color.White;
            street.Clicked += HandleClicked;
            hybrid.Clicked += HandleClicked;
            satellite.Clicked += HandleClicked;
            var parts = new StackLayout
            {
                Spacing = 30,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Orientation = StackOrientation.Horizontal,
                Children = { street, hybrid, satellite }
            };
            var stack = new StackLayout { Spacing = 10};
            stack.Children.Add(MyMap);
            stack.Children.Add(parts);
            stack.Children.Add(DestinationPicker);
            Content = stack;

            

        }
        void HandleClicked(object sender, EventArgs e)
        {
            var b = sender as Button;
            switch (b.Text){
                case " Street ":
                    MyMap.MapType = MapType.Street;
                    break;
                case " Hybrid ":
                    MyMap.MapType = MapType.Hybrid;
                    break;
                case " Satellite ":
                    MyMap.MapType = MapType.Satellite;
                    break;
            }
        }
        private void PlaceAMarker()
        {
            //Cal State San Marcus
            var position = new Position(33.12974, -117.1586638);
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "CSUSM",
                Address = "333 N Twin Oaks Valley Rd, San Marcos, CA 92096"
            };
            MyMap.Pins.Add(pin);
            //Manna
            var position1 = new Position(33.134443, -117.119166);
            var pin1 = new Pin
            {
                Type = PinType.Place,
                Position = position1,
                Label = "Manna Barbeque",
                Address = "740 Nordahl Rd, San Marcos, CA 92069"
            };
            MyMap.Pins.Add(pin1);
            //Wings and Things
            var position2 = new Position(33.129282, -117.16547000000003);
            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "Wings And Things",
                Address = "324 S Twin Oaks Valley Rd, San Marcos, CA 92078"
            };
            MyMap.Pins.Add(pin2);
            //A random park
            var position3 = new Position(33.139907, -117.16919330000002);
            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "Conners Park",
                Address = "320 W San Marcos Blvd, San Marcos, CA 92069"
            };
            MyMap.Pins.Add(pin3);
            //Hookah Lounge, Nara Hookah
            var position4 = new Position(33.144792, -117.16193699999997);
            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = position4,
                Label = "Nara Hookah Lounge",
                Address = "403 N Twin Oaks Valley Rd #115, San Marcos, CA 92069"
            };
            MyMap.Pins.Add(pin4);
        }
        private void PopulatePicker()
        {
            var Locations = new List<string>();
            Locations.Add("CSUSM");
            Locations.Add("Manna Barbeque");
            Locations.Add("Wings And Things");
            Locations.Add("Conners Park");
            Locations.Add("Nara Hookah Lounge");
            DestinationPicker.ItemsSource = Locations;

        }
        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if(DestinationPicker.SelectedIndex == 0)
            {
                var location = MapSpan.FromCenterAndRadius(new Position(33.12974, -117.1586638), Distance.FromMiles(1));
                MyMap.MoveToRegion(location);
            }
            else if(DestinationPicker.SelectedIndex == 1)
            {
                var location1 = MapSpan.FromCenterAndRadius(new Position(33.134443, -117.119166), Distance.FromMiles(1));
                MyMap.MoveToRegion(location1);
            }
            else if(DestinationPicker.SelectedIndex == 2)
            {
                var location2 = MapSpan.FromCenterAndRadius(new Position(33.129282, -117.16547000000003), Distance.FromMiles(1));
                MyMap.MoveToRegion(location2);
            }
            else if (DestinationPicker.SelectedIndex == 3)
            {
                var location3 = MapSpan.FromCenterAndRadius(new Position(33.139907, -117.16919330000002), Distance.FromMiles(1));
                MyMap.MoveToRegion(location3);
            }
            else{
                var location4 = MapSpan.FromCenterAndRadius(new Position(33.144792, -117.16193699999997), Distance.FromMiles(1));
                MyMap.MoveToRegion(location4);
            }
        }
    }
}
