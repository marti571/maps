﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Destinations
{
    public partial class DestinationsPage : ContentPage
    {
        public DestinationsPage()
        {
            InitializeComponent();

        }

        void Maps_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new MapsPage());
        }


    }
}